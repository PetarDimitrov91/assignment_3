create table IF NOT EXISTS actor
(
    id        bigint not null
        primary key ,
    alias     varchar(255),
    full_name varchar(255),
    gender    varchar(255),
    picture   varchar(255)
);



create table IF NOT EXISTS franchise
(
    id          bigint not null
        primary key,
    description varchar(255),
    name        varchar(255)
);


create table IF NOT EXISTS movie
(
    id           bigint not null
        primary key,
    director     varchar(255),
    genre        varchar(255),
    picture      varchar(255),
    release_year varchar(255),
    title        varchar(255),
    trailer      varchar(255)
);


-- auto-generated definition
create sequence IF NOT EXISTS movie_sequence
    start 1;
create sequence IF NOT EXISTS actor_sequence start 1;
create sequence IF NOT EXISTS franchise_sequence start 1;
create sequence IF NOT EXISTS actors_movies_sequence start 1;
/*
 Franchises
 */

INSERT INTO franchise (id, name, description)
VALUES (1, 'Marvel', 'The Marvel universe is awesome!');
INSERT INTO franchise (id, name, description)
VALUES (2, 'Harry Potter', 'The Harry Potter franchise is awesome!');
INSERT INTO franchise (id, name, description)
VALUES (3, 'James Bond', 'The James Bond franchise is awesome!');
/*
 Movies
 */
INSERT INTO movie (id, title, genre, release_year, director, picture, trailer, franchise_id)
VALUES (1, 'Iron Man', 'Action', '2008', 'John Favro', 'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSzFXKSHWb3-5LAMWLxGAB5HzqsefGX4eYINaSHLd9JDNRu-LiM','https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwi-oveZiqr2AhXFgv0HHXK5D58Q3yx6BAgfEAI&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D0mbO5NA6dAg&usg=AOvVaw2I_VqLld3ATkm9FK7Hq9Pv', 1);
INSERT INTO movie (id, title, genre, release_year, director, picture, trailer, franchise_id)
VALUES (2, 'Harry Potter and the Philosophers Stone', 'Fantasy, Adventure', '2002', 'Chris Columbus', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSxlN5yVLJnpmTyOp33mZ7PRtTs9KqiKmq2pOczXX0Qu_E4SrTY','https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwiWoqDIi6r2AhWigv0HHUy2CCUQ3yx6BAgHEAI&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DVyHV0BRtdxo&usg=AOvVaw2HbOpLaiH5zGLkwuATgpLr', 2);
INSERT INTO movie (id, title, genre, release_year, director, picture, trailer, franchise_id)
VALUES (3, 'James Bond for Real', 'Action', '2006', 'Rob Done',  'https://www.themoviedb.org/t/p/w600_and_h900_bestv2/vYNYCv3LrZjRvQaFTm5HnZVCIut.jpg','https://www.themoviedb.org/t/p/w600_and_h900_bestv2/vYNYCv3LrZjRvQaFTm5HnZVCIut.jpg', 3);
/*
 Characters
 */
INSERT INTO actor (id, full_name, alias, gender, picture)
VALUES (1, 'Robert Downey Jr.', 'no', '0', 'http://t1.gstatic.com/licensed-image?q=tbn:ANd9GcQ1k-ZKUe_s6cK7TnwlXGbThAFV-f1XPHTMoriO4Hbq0nhP3_fO3mcikmWF72sw');
INSERT INTO actor (id, full_name, alias, gender, picture)
VALUES (2, 'Daniel Radcliff', 'Stone-man', '0', 'https://www.themoviedb.org/person/10980-daniel-radcliffe?language=en');
INSERT INTO actor (id, full_name, alias, gender, picture)
VALUES (3, 'Daniel Craig', '007', '0', 'https://www.themoviedb.org/t/p/w600_and_h900_bestv2/vYNYCv3LrZjRvQaFTm5HnZVCIut.jpg');


/*
 Link characters to movies
 */

/*
 Movie 1
 */
insert into actors_movies (id,actor_id, movie_id) values(nextval('hibernate_sequence'),1,1);

/*
 Movie 2
 */
insert into actors_movies (id,actor_id, movie_id) values(nextval('hibernate_sequence'),1,2);
insert into actors_movies (id,actor_id, movie_id) values(nextval('hibernate_sequence'),2,2);


